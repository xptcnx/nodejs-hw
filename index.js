const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const fileRouter = require('./fileRouter');
const app = express();

app.use(express.json());
const logFile = fs.createWriteStream(path.join(__dirname, 'morgan.log'), { flags: 'a' });
app.use(morgan('tiny', { stream: logFile }));

app.use('/api/files', fileRouter);

app.all('*', (req, res) => {
  return res.status(400).json({ message: 'Bad request.' });
});

const PORT = process.env.PORT || 8080;
  app.listen(PORT, () => {
    console.log('Server work at port 8080!');
  });