const fs = require('fs').promises;
const fsSync = require('fs');
const path = require('path');

const createFile = async (req, res) => {
  const { filename, content } = req.body;

  try {
    await fs.writeFile(`./files/${filename}`, `${content}`, 'utf-8');
    res.status(200).json({ message: 'File created successfully' });
  } catch {
    res.status(500).json({ message: 'Server error' });
  }
};

const getFiles = async (req, res) => {
  try {
    if (fsSync.existsSync('./files')) {
      const files = await fs.readdir('./files');
      res.status(200).json({
        message: 'Success',
        files: files,
      });
    } else {
      return res.status(400).json({ message: 'No files uploaded yet' });
    }
  } catch {
    res.status(500).json({ message: 'Server error' });
  }
};

const getFileByName = async (req, res) => {
  const filename = req.params.filename;
  const fileExt = path.extname(filename);

  if (!fsSync.existsSync(`./files/${filename}`)) {
    return res
      .status(400)
      .json({ message: `No file with ${filename} filename found` });
  }

  try {
    const { birthtime } = await fs.stat(`./files/${filename}`);
    const content = await fs.readFile(`./files/${filename}`, 'utf-8');
    const result = {
      message: 'Success',
      filename: filename,
      content: content,
      extension: fileExt,
      uploadedDate: birthtime,
    };
    res.status(200).json(result);
  } catch {
    res.status(500).json({ message: 'Server error' });
  }
};

const updateFile = async (req, res) => {
  const filename = req.params.filename;
  const content = req.body.content;
  try {
    await fs.writeFile(`./files/${filename}`, `${content}`, 'utf-8');

    res.status(200).json({
      message: 'File has been updated',
    });
  } catch {
    res.status(500).json({ message: 'Server error' });
  }
};

const deleteFile = async (req, res) => {
  const filename = req.params.filename;
  try {
    await fs.unlink(`./files/${filename}`);
    res.status(200).json({ message: 'File has been deleted' });
  } catch {
    res.status(500).json({ message: 'Server error' });
  }
};

module.exports = {
  createFile,
  getFiles,
  getFileByName,
  updateFile,
  deleteFile,
};
